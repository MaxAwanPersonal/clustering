__author__ = 'max'
import cv2
import numpy as np
import os
from string import ascii_uppercase
from time import time


class SceneProcessor():

    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    SEQ_DIR = None
    frame_threshold = 5000000
    scene_threshold = 10
    scene_hsv_threshold = 50
    last_pic = None
    current_scene = None
    scenes = []
    letter_index = 1
    output = []
    display_frames = False
    process_scenes = True
    num_images = 985

    def __init__(self, display_frames=False, process_scenes=True, sequence=1):
        """Sets the scene processor up with the relevant settings for either sequence and then starts"""
        self.display_frames = display_frames
        self.process_scenes = process_scenes
        if not sequence == 2:
            folder_name = "Sequence1"
            filename = "sequence1.csv"
        else:
            folder_name = "Sequence2"
            filename = "sequence2.csv"
            self.num_images = 2771
        self.SEQ_DIR = os.path.join(self.BASE_DIR, folder_name)
        self.play_sequence(filename=filename)

    def play_sequence(self, filename="sequence.csv"):
        """
        Loads the files iteratively and can optionally process them (detecting scene changes) and/or display them
        """
        t0 = time()
        if self.process_scenes:
            self.current_scene = Scene()
        self.output.append((1, 'A'))
        for i in range(1, self.num_images):
            filename = "%06d.jpg" % i
            file = os.path.join(self.SEQ_DIR, filename)
            pic = cv2.imread(file, 3)
            if self.process_scenes:
                grey_pic = cv2.imread(file, 0)
                if i > 1:
                    # If the absolute difference in pixel values between consecutive greyscale frames
                    # is over the threshold, it will be treated as a potential scene change
                    diff_from_last = cv2.absdiff(grey_pic, self.last_pic)
                    if np.sum(diff_from_last) < self.frame_threshold:
                        # If under the threshold. the frame number will be associated with the current scene
                        self.add_frame_to_scene(i)
                    else:
                        # If over the threshold, treat as a potential scene change
                        self.finalize_scene(i, grey_pic, col_pic=pic)
                        #Add the new frame to either the same scene, an alternate existing scene or a new one
                        self.add_frame_to_scene(i)
                else:
                    #The first frame automatically gets added to the scene
                    self.add_frame_to_scene(i)
                self.last_pic = grey_pic

            if self.display_frames:
                #Optional viewing of footage
                cv2.imshow('Img', pic)
                k = cv2.waitKey(100)
                if k == 27:
                    break
        t1 = time()
        if self.process_scenes:
            fps = self.num_images/(t1-t0)
            print 'FPS: %2.2f' % fps
            self.output_to_csv(filename=filename)
        else:
            duration = t1-t0
            print "Duration: %2.2f" % duration

    def add_frame_to_scene(self, i):
        """Add the frame number to the current scene"""
        self.current_scene.add_frame(i)

    def finalize_scene(self, i, pic, col_pic):
        """
        Store the last frame of the scene on the object and
        perform a more in depth comparison with all scenes including the current one to verify scene change
        """
        last_index = i-1
        filename = "%06d.jpg" % last_index
        last_pic = cv2.imread(os.path.join(self.SEQ_DIR, filename), 0)
        last_col_pic = cv2.imread(os.path.join(self.SEQ_DIR, filename), 3)

        #Store the last frame in greyscale and color onto the scene object for future comparison
        self.current_scene.finalize(last_pic, last_col_pic)
        if not self.current_scene in self.scenes:
            self.scenes.append(self.current_scene)
        se = self.scene_exists(col_pic) # Checks whether a color version of the current frame is similar to the last frame of any scenes
        if se == -1:
            # If the current frame does not match any existing scenes, create a new scene and label it.
            self.current_scene = Scene(letter=ascii_uppercase[self.letter_index])
            self.letter_index += 1
        else:
            if self.current_scene.get_letter() == self.scenes[se].get_letter():
                # If the color version of the current frame matches the current scene,
                # skip the rest and continue processing.
                return False
            # If the current frame matches an existing scene, set that as the current scene
            self.current_scene = self.scenes[se]
        print self.current_scene.get_letter()
        #Add a tuple of (keyframe, scene) to the output. The output will be written to the csv on completion.
        self.output.append((i, self.current_scene.get_letter()))
        return True

    def scene_exists(self, col_pic):
        """Compare colored frame to averages of existing scenes.
         Returns -1 if frame does not match or the scene number if it does"""
        potential_scenes = []

        for n, sc in enumerate(self.scenes):
            #Looks at the absolute differences in average pixel value
            #  and hsv between current frame and the last frame of every scene
            diff = abs(self.compute_frame_average(col_pic)-self.compute_frame_average(sc.last_col_pic))
            hsv_diff = self.hsv_difference(col_pic, sc.last_col_pic)
            #If both the absolute differences are over their thresholds, include in a shortlist of potential scenes.
            if diff < self.scene_threshold and hsv_diff<self.scene_hsv_threshold:
                potential_scenes.append((n, diff))
        if len(potential_scenes) == 0:
            print "new scene"
            return -1
        else:
            best_fit = min(potential_scenes, key=lambda t: t[1])
            #Return the scene with the lowest absolute difference from the shortlist
            print "existing scene"
            return best_fit[0]

    @staticmethod
    def hsv_difference(pic, last_pic):
        """Converts the color images to hsv and returns the sum of the absolute differences"""
        curr_hsv = cv2.split(cv2.cvtColor(pic, cv2.COLOR_BGR2HSV))
        last_hsv = cv2.split(cv2.cvtColor(last_pic, cv2.COLOR_BGR2HSV))
        delta_hsv = [-1, -1, -1]
        for i in range(3):
            num_pixels = curr_hsv[i].shape[0] * curr_hsv[i].shape[1]
            curr_hsv[i] = curr_hsv[i].astype(np.int32)
            last_hsv[i] = last_hsv[i].astype(np.int32)
            delta_hsv[i] = np.sum(np.abs(curr_hsv[i] - last_hsv[i])) / float(num_pixels)
        return sum(delta_hsv) / 3.0

    @staticmethod
    def compute_frame_average(frame):
        """Computes the average pixel value/intensity over the whole frame.
        The value is computed by adding up the 8-bit R, G, and B values for
        each pixel, and dividing by the number of pixels multiplied by 3.
        Returns:
            Floating point value representing average pixel intensity.
        """
        num_pixel_values = float(
            frame.shape[0] * frame.shape[1] * frame.shape[2])
        avg_pixel_value = np.sum(frame[:, :, :]) / num_pixel_values
        return avg_pixel_value

    def output_to_csv(self, filename):
        """Outputs the data to csv format"""
        import csv
        with open('sequence2.csv', 'wb') as csvfile:
            cwriter = csv.writer(csvfile, delimiter=',', quotechar='"')
            cwriter.writerow(['keyframe', 'scene id'])
            for line in self.output:
                a = str(line[0])
                b = str(line[1])
                value_list = [a, b]
                cwriter.writerow(value_list)
        csvfile.close()


class Scene():
    letter = None
    frames = []
    last_pic = None
    last_col_pic = None
    last_hsv = None

    def __init__(self, letter="A"):
        self.letter=letter

    def add_frame(self, i):
        """Associates a frame number with the scene"""
        self.frames.append(i)

    def finalize(self, last_pic, last_col_pic):
        """When scene changes, this updates the info for the previous scene"""
        self.last_pic = last_pic
        self.last_col_pic = last_col_pic
        self.last_hsv = cv2.split(cv2.cvtColor(last_col_pic, cv2.COLOR_BGR2HSV))

    def get_letter(self):
        """Returns the letter corresponding to the scene"""
        return self.letter


def getopts(argv):
    """Taken from stack overflow for convenience. This parses input arguments nicely"""
    opts = {}  # Empty dictionary to store key-value pairs.
    while argv:  # While there are arguments left to parse...
        if argv[0][0] == '-':  # Found a "-name value" pair.
            if argv[1]:
                opts[argv[0]] = argv[1]  # Add key and value to the dictionary.
            else:
                opts[argv[0]] = None
        argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
    return opts

if __name__ == '__main__':
    from sys import argv
    myargs = getopts(argv)
    if '-s' in myargs:  # Example usage.
        sequence = int(myargs['-s'])
    else:
        sequence = 1
    if '-df' in myargs:
        display_frames = True if myargs['-df'] == "True" else False
    else:
        display_frames = False
    if '-pf' in myargs:
        process = False if myargs['-pf'] == "False" else True
    else:
        process = True
    sp = SceneProcessor(display_frames=display_frames, process_scenes=process, sequence=sequence)
