Scene change detection


Instructions
------------
Create a folder named Sequence1 and add all the sequence 1 images into it

Create a folder named Sequence2 and all the sequence 2 images into it

To process the files for sequence 1 and output a csv type:

python cluster.py -s 1

To do the same for sequence 2, type:

python cluster.py -s 2

To display the frames as it processes, add the argument -df True

e.g. python cluster.py -df True

If you want to just watch the frames without processing, you will also need to add -pf False

e.g. python cluster.py -df True -pf False

If the files are processed then a csv will be outputted into the folder named either "sequence1.csv" or "sequence2.csv"

By default, the algorithm will process the files for sequence 1 without displaying the frames
